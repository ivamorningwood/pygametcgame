import sys
import jsonhandler, graphics, button,deck, cards

#Global Variables
PG = graphics.pygame
clock = 0
lost = False
gameStarted = False
playerTurn = 0

#Methods
def GenerateCardDeck(deck, deckData):
    for item in deckData:
        currentCard = cards.Card(item["name"],item["cost"],
                                 item["icon"],item["health"],
                                 item["damage"],item["damageType"],
                                 item["immunities"],item["action"])
        deck.add(currentCard)
    #deck.shuffle()

def AddCardsToHand(deck,hand,amt):
    deck.deal(hand,amt)

def CreateCardButtons(hand,positionY):
    currentX = graphics.positionX
    i = 0
    buttons = []
    while i < 5:
        currentButton = button.CardButton(hand, i,
                                          currentX,positionY,
                                          graphics.cardWidth,
                                          graphics.cardHeight)
        buttons.append(currentButton)
        currentX = graphics.GetWidth(currentX)
        i += 1
    return buttons

def DrawCardButtons(hand):
    for b in hand:
        b.Draw()

def EndPlayerTurn():
    playerTurn = 1

#Main Game Code
#Init pygame and graphics
PG.init()
graphics.init()
graphics.LoadImages()
clock = PG.time.Clock()

#Create our Decks
PlayerDeck = deck.Deck()
deckName = "noforkfamily.json"
graphics.LoadImages()
loadedDeck = jsonhandler.LoadFile(deckName)
GenerateCardDeck(PlayerDeck,loadedDeck)
#Add our decks to our list for our actions
button.PlayerActions.decks[0] = PlayerDeck

#create our buttons
PlayerActiveHand = CreateCardButtons(0,graphics.playerActivePositionY)
#add our hands to our list of hands for our actions
button.PlayerActions.cardHands.append(PlayerActiveHand)
PlayerHand = CreateCardButtons(1,graphics.playerHandPositionY)
button.PlayerActions.cardHands.append(PlayerHand)
AIActiveHand = CreateCardButtons(2,graphics.AIActivePositionY)
button.PlayerActions.cardHands.append(AIActiveHand)
#AIHand = CreateCardButtons()

#Create buttons for Decks
PlayerDeckButton = button.Button(graphics.cardBack,graphics.deckPositionX,
                                 graphics.playerDeckPositionY,
                                 graphics.cardWidth,graphics.cardHeight,
                                 "PlayerDeckClicked")

#create buttons for the player and AI
PlayerButton = button.PlayerButton(0,graphics.deckPositionX,
                                 graphics.playerPositionY,
                                 graphics.cardWidth,graphics.cardHeight)

AIButton = button.PlayerButton(1,graphics.offset,
                                 graphics.AIActivePositionY,
                                 graphics.cardWidth,graphics.cardHeight)

#Create the graphics for the AI Hand
AIHand = [graphics.cardHolder,graphics.cardHolder,graphics.cardHolder,graphics.cardHolder,graphics.cardHolder,]

#references to our player and AI files
Player = button.PlayerActions.player
AI = button.PlayerActions.AI

while not lost:
    #Handle Events
    for event in PG.event.get():
        #Global Events
        if event.type == PG.QUIT:
            lost = True
        #Keyboard Inputs
        
        # Other Event Receivers
        if playerTurn == 0:
            PlayerButton.HandleEvents(event)
            AIButton.HandleEvents(event)
            PlayerDeckButton.HandleEvents(event)
            for e in PlayerHand:
                e.HandleEvents(event)
            for e in PlayerActiveHand:
                e.HandleEvents(event)
        else:
            playerTurn = AI.PerformActions()
    
    #Update Objects

    #Screen Draws
    graphics.DrawBackground()
    #Player Items
    DrawCardButtons(PlayerActiveHand)
    DrawCardButtons(PlayerHand)    
    PlayerDeckButton.Draw()
    graphics.DrawPlayer(Player.Health, Player.ActionPoints)
    #AI Items
    DrawCardButtons(AIActiveHand)
    graphics.DrawAI(AI.Health, AI.ActionPoints)
    graphics.ShowImage(graphics.cardBack,graphics.offset,graphics.offset)
    graphics.DrawAIHand(AIHand)

    PG.display.update()
    clock.tick(60)

PG.quit()
quit()
