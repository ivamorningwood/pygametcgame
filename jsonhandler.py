import json

filePath = "Decks/"

def LoadFile(fileName):
    with open(filePath+fileName, "r") as read_file:
        data = json.load(read_file)
    return data

def SaveFile(fileName, data):
    with open(filePath+fileName, "w") as write_file:
        json.dump(data, write_file)

