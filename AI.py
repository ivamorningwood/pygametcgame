Health = 15
MAXHEALTH = Health
ActionPoints = 7
MAXACTIONPOINTS = ActionPoints


def ResetAP():
    ActionPoints = MAXACTIONPOINTS

def IncreaseHealth(amt):
    global MAXHEALTH
    MAXHEALTH += amt

def CheckActionPoints(amt):
    return True if amt < ActionPoints else False

def ChangeAP(amt):
    global ActionPoints
    ActionPoints += amt

def PerformActions():
    print("Performing AI Actions")
    if ActionPoints == 0:
        return 0
    else:
        return 1