import pygame

screen_size = (1200,800)
screen = pygame.display.set_mode(screen_size)
defaultFont = ""
mouse_position = (0,0)
caption = "Trailer Park Titans"

#Card Variables
cardWidth = 150
cardHeight = 180
cardHolder = ""
offset = 15

#Default Colors
BLACK = (0,0,0)
WHITE = (255,255,255)

#Image variables
#Change these per project
image_path = "images/"
image_names = ["Redneck","Banjo","Inlaws","Philosopher",
                "CousinBobby","GrandPa","MasterChef","BeautyQueen","Princess"]
image_dictionary = {}
background = pygame.image.load('images/BG/battleback3.png')#background
cardHolder = pygame.image.load('images/BG/plain-square.png') #background for our card squares
cardBack = pygame.image.load('images/BG/bus.png') #Background for card deck
playerBack = pygame.image.load('images/BG/playercircle2.png') #background for the player button
#transform the background to screen size
background = pygame.transform.scale(background, screen_size)
#transform card holder image to size of a card
cardHolder = pygame.transform.scale(cardHolder, (cardWidth,cardHeight))
#transform the deck image to screen size
cardBack = pygame.transform.scale(cardBack, (cardWidth,cardHeight))
#transform the player image to screen size
playerBack = pygame.transform.scale(playerBack, (cardHeight,cardHeight))

#Position Variables used to set the cards in the right spot at the beginning of the game
playerActivePositionY = int((screen_size[1]/2 - (cardHeight*2))/3 + screen_size[1]/2)
playerHandPositionY = int(playerActivePositionY + cardHeight + offset)
positionX = int(screen_size[0]/2 - (cardWidth*2 + cardWidth /2 + offset *2))
deckPositionX = screen_size[0] - cardWidth - offset
playerPositionY = screen_size[1] - cardHeight * 2 - offset * 2
playerDeckPositionY = screen_size[1] - cardHeight - offset
AIActivePositionY = offset * 2 + cardHeight



#Image Display Methods
def init():
    screen = pygame.display.set_mode(screen_size)
    defaultFont = pygame.font.Font('freesansbold.ttf',15)
    pygame.display.set_caption(caption)
    
def LoadImages():
    for i in image_names:
        current = pygame.image.load(image_path+i+'.png')
        current = pygame.transform.scale(current, (cardWidth,cardHeight))
        AddImageToDictionary(i,current)
    print(len(image_dictionary))

def AddImageToDictionary(name,image):
    image_dictionary[name] = image

def ShowImage(img,x,y):
    screen.blit(img,(x,y))

def DrawBackground():
    if background == "":
        screen.fill(BLACK)
    else:
        ShowImage(background,0,0)

def TextObjects(text, font):
    textSurface = font.render(text, True, WHITE)
    return textSurface, textSurface.get_rect()

def DrawPlayer(health,ap):
    info = str(health) + " : " + str(ap)
    ShowImage(playerBack,deckPositionX - 15,playerPositionY)
    text = pygame.font.Font('freesansbold.ttf',32)
    surf,rect = TextObjects(info,text)
    centerPointX = deckPositionX + cardWidth/2
    centerPointY = playerPositionY + cardHeight/2
    rect.center = (centerPointX,centerPointY)
    screen.blit(surf, rect)

def DrawAI(health,ap):
    info = str(health) + " : " + str(ap)
    print(info)
    ShowImage(playerBack,offset,AIActivePositionY)
    text = pygame.font.Font('freesansbold.ttf',32)
    surf,rect = TextObjects(info,text)
    centerPointX = offset + 5 + cardWidth/2
    centerPointY = AIActivePositionY + cardHeight/2
    rect.center = (centerPointX,centerPointY)
    screen.blit(surf, rect)

def DrawAIHand(hand):
    currentX = positionX
    i = 0
    while i < 5:
        ShowImage(hand[i],currentX,offset)
        currentX = GetWidth(currentX)
        i += 1
    

def GetWidth(currentX):
    return currentX + cardWidth + offset
