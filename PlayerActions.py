'''File for holding player actions in a TCG'''
###########################################################
###### Do Not Delete This Stuff
###########################################################
import player, AI

#Variables for keeping track of the action we should take
currentCard = None
currentCardHand = None
currentTarget = None
currentTargetHand = None
currentAction = None
cardHands = []


#Methods for passing card and deck information to actions
decks = ["",""]

#Keep track of our players
Players = [player,AI]
currentPlayer = 0

#handles when the player deck is clicked on
def PlayerDeckClicked():
    global currentCard,currentCardHand,currentTarget,currentTargetHand
    if currentCard == None:
        currentCard = decks[0].cards[0]
    else:
        currentCard = None

#resets the data after you have performed your action
def ResetData():
    global currentCard,currentCardHand,currentTarget,currentTargetHand
    currentCard= currentCardHand= currentTarget= currentTargetHand = currentAction = None

def CheckAPAmount(index):
    return Players[index].CheckActionPoints(currentCard.cost)

def RemoveAP(index):
    print(currentCard.cost)
    print(index)
    Players[index].ChangeAP(-currentCard.cost)

def DamagePlayer(index):
    if CheckAPAmount(currentPlayer):
        Players[index].Health -= currentCard.damage
        RemoveAP(currentPlayer)


#Your Functions Down Here. Use these as an example
def Rolling_Pin():
    # currentTarget.ModifyHealth(currentCard.damage)
    # Player.ModifyActionPoints(currentCard.cost)
    pass

def Babble():
    print(currentCard.damage)


    


