import graphics, PlayerActions

class Button():   
    def __init__(self, image, x=0, y=0, width=100, height=50, action=None):
        self.image = image
        self.rect = self.image.get_rect()
        self.action = action
        if action != None:
            self.action = getattr(PlayerActions,self.action)
        
        # you can't use it before `blit` 
        self.rect.topleft = (x, y)

        self.hovered = False
        #self.clicked = False

    def Update(self):

        if self.hovered:
            pass
        else:
            pass

    def Draw(self):
        graphics.screen.blit(self.image, self.rect)

    def HandleEvents(self, event):
        if event.type == graphics.pygame.MOUSEMOTION:
            self.hovered = self.rect.collidepoint(event.pos)
        elif event.type == graphics.pygame.MOUSEBUTTONDOWN:
            if self.hovered:
                if self.action:
                    self.action()

class CardButton(Button):
    def __init__(self, hand, index, x=0, y=0, width=100, height=50,  card = None):
        self.card = card
        self.hand = hand
        self.index = index
        self.image = graphics.cardHolder
        self.action = None
        if card != None:
            self.image = self.card.icon
            self.action = self.card.action
            
        Button.__init__(self,self.image,x,y,width,height, self.action)

    def SetAction(self, action = None):
        if action != None:
            action = getattr(PlayerActions,action)
        return action 

    def HandleEvents(self, event):
        if event.type == graphics.pygame.MOUSEMOTION:
            self.hovered = self.rect.collidepoint(event.pos)
        elif event.type == graphics.pygame.MOUSEBUTTONDOWN:
            if self.hovered:
                if PlayerActions.currentCard == None:
                    PlayerActions.currentCard = self.card
                    PlayerActions.currentCardHand = PlayerActions.cardHands[self.hand]
                    if self.hand < 2:
                        PlayerActions.currentPlayer = 0
                    else:
                        PlayerActions.currentPlayer = 1
                    PlayerActions.currentAction = self.action
                elif PlayerActions.currentCard == PlayerActions.decks[0].cards[0]:
                    self.ChangeCard(PlayerActions.decks[0].cards[0])
                    del(PlayerActions.decks[0].cards[0])
                    PlayerActions.ResetData()
                else:
                    if self.card == None:
                        if self.hand < 2:
                            self.ChangeCard(PlayerActions.currentCard)
                            self.ButtonReset()
                    else:
                        PlayerActions.currentTarget = self.card
                        PlayerActions.currentTargetHand = self.hand
                        PlayerActions.currentAction()
                    PlayerActions.ResetData()

    def ChangeCard(self,card):
        self.card = card
        self.image = graphics.image_dictionary[self.card.icon]
        self.action = self.SetAction(card.action)
    
    def ButtonReset(self):        
        for i in PlayerActions.currentCardHand:
            if i.card == PlayerActions.currentCard:
                i.card = None
                i.image = graphics.cardHolder
                i.action = None
                break

    
class PlayerButton(Button):

    def __init__(self, character, x=0, y=0, width=100, height=50):
        self.character = character
        self.image = graphics.playerBack
        self.action = None
            
        Button.__init__(self,self.image,x,y,width,height, self.action)

    def HandleEvents(self, event):
        if event.type == graphics.pygame.MOUSEMOTION:
            self.hovered = self.rect.collidepoint(event.pos)
        elif event.type == graphics.pygame.MOUSEBUTTONDOWN:
            if self.hovered:
                if PlayerActions.currentCard != None:
                    PlayerActions.DamagePlayer(self.character)
                    
                    
                


