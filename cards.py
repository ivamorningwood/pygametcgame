# Cards Module
# Basic classes for a game with playing cards

class Card(object):

    name = ""
    cost = 0
    icon = ""
    health = 0
    damage = 0
    damageType = ""
    isDazed = False
    immunities = {}
    action = ""
    

    def __init__(self,n,c,i,h,d,dt,imm,a):
        self.name = n
        self.cost = c
        self.icon = i
        self.health = h
        self.damage = d
        self.damageType = dt
        self.immunities = imm
        self.action = a

    def ModifyHealth(self, val, dt):
        if immunities[dt] == True:
            return
        self.health += val

    def SetImmunity(self,dt):
        immunities[dt] == True

    def SetDazed(self,val = True):
        isDazed = val

        

if __name__ == "__main__":
    print ("This is a module with classes for playing cards.")
    deck = Deck()
    input("\n\nPress the enter key to exit.")
