class Hand(object):
    """ A hand of cards. """
    def __init__(self):
        self.cards = []

    def clear(self):
        self.cards = []

    def add(self, card):
        self.cards.append(card)

    def give(self, card, other_hand):
        self.cards.remove(card)
        other_hand.add(card)


class Deck(Hand):
    """ A deck of cards. """
    def populate(self, deck):
        for suit in Card.SUITS:
            for rank in Card.RANKS: 
                self.add(Card(rank, suit))

    def shuffle(self):
        import random
        random.shuffle(self.cards)

    def deal(self, hand, per_hand = 1):
        for rounds in range(per_hand): 
            if self.cards:
                top_card = self.cards[0]
                self.give(top_card, hand)
            else:
                print ("Can't continue deal. Out of cards!")

if __name__ == "__main__":
    print ("This is a module with classes for playing cards.")
    deck = Deck()
    input("\n\nPress the enter key to exit.")
